/*-------------------------------------------------
 * GULP's FILE CONFIGURATION
 --------------------------------------------------*/
//	CONTENT THIS FILE:
//      # DEFAULT
//		  # CACHETEMPLATES
//      # CLEAN
//        * CLEAN:CSS
//        * CLEAN:FONT
//        * CLEAN:SCRIPT
//        * CLEAN:TEMPLATE
//        * CLEAN:ASSETS
//      # CONSOLE
//        * CONSOLE:GLP
//      # COPY
//        * COPY:IMG
//        * COPY:MEDIA
//        * COPY:FAVICON
//        * COPY:FONTS
//        * COPY:LIBSJS
//      # CSS
//      # DIST
//        * DIST:MIN
//        * DIST:NORMAL
//		  # JS
//      # SCSS
//      # SERVE
//		  # TEMPLATES
//		  # WATCH
//
//  TASK GROUP
//      # DEVELOP
//      # DIST
//      # CLEAN:ALL
//      # COPY:ALL
//      # BUILD



(function() {
	'use strict';
/*-------------------------------------------------
* LIB'S REQUIRE
*-------------------------------------------------*/
	const GULP	= require('gulp-help')(require('gulp'));
	const FOLDER 	= require('./gulp/paths/folder.js');
  const UTILS = require('./gulp/utils/utils.js');
	const GLP 	= require('gulp-load-plugins')({
		scope: ['devDependencies'],
		pattern: '*'
	});






/*-------------------------------------------------
*       DECLARATE CONFIG VAR
*------------------------------------------------*/
	var
    gCommon,
    gTemplates,
    gScss,
    gJs,
    gServer,
    gCssmin;

    /*
      Declaracion del objeto COMMON. En este objeto se incluirá todas aquellas opciones que puedan ser communes en varias tareas.
    */
      gCommon = {
        nameCache: 'html',
        eslint: {
          configFile: './config/lint/.eslintrc'
        },
        proxy: 'http://10.0.8.102:8080'
      };






/*-------------------------------------------------
*       IMPORT AND CONFIG TASK
*------------------------------------------------*/


    /*----------------------------------
	  * TASK # CACHETEMPLATES
	  ----------------------------------*/
      require('./gulp/task/cache__templates')(GULP, GLP, FOLDER, gCommon);




    /*----------------------------------
	  * TASK # CLEAN
	  ----------------------------------*/
      require('./gulp/task/clean__css')(GULP, GLP, FOLDER);
      require('./gulp/task/clean__fonts')(GULP, GLP, FOLDER);
      require('./gulp/task/clean__script')(GULP, GLP, FOLDER);
      require('./gulp/task/clean__templates')(GULP, GLP, FOLDER);
      require('./gulp/task/clean__assets')(GULP, GLP, FOLDER);




    /*----------------------------------
	  * TASK # CONSOLE
	  ----------------------------------*/
      require('./gulp/task/console__glp')(GULP, GLP);





    /*----------------------------------
	  * TASK # COPY
	  ----------------------------------*/
  	  require('./gulp/task/copy__favicon')(GULP, GLP, FOLDER);
      require('./gulp/task/copy__fonts')(GULP, GLP, FOLDER);
      require('./gulp/task/copy__img')(GULP, GLP, FOLDER);
      require('./gulp/task/copy__media')(GULP, GLP, FOLDER);
      require('./gulp/task/copy__libsjs')(GULP, GLP, FOLDER);
      require('./gulp/task/copy__libscss')(GULP, GLP, FOLDER);




    /*----------------------------------
	  * TASK # CSS
	  ----------------------------------*/
      gCssmin = {
        compatibility: 'ie9'
      };
      require('./gulp/task/css__min')(GULP, FOLDER, GLP, gCssmin);




    /*----------------------------------
	  * TASK # DIST
	  ----------------------------------*/
      require('./gulp/task/dist__normal')(GULP, GLP, FOLDER);
      require('./gulp/task/dist__min')(GULP, GLP, FOLDER);





    /*----------------------------------
	  * TASK # JS
	  ----------------------------------*/
      gJs = {
        fileDev   :   'front-script-dev.js',
        fileDist  :   'front-script.js',
        eslint: {
          configFile: './config/lint/.eslintrc',
          format: './node_modules/eslint-path-formatter'
        }
      };
      require('./gulp/task/js')(GULP, FOLDER, GLP, gJs, gCommon);
      require('./gulp/task/js__min')(GULP, FOLDER, GLP);






    /*----------------------------------
	  * TASK # SCSS
	  ----------------------------------*/
      gScss ={
        optScss: {
          outputStyle: 'extend'
        },
        autoprefix: {
          browsers  : ['last 2 versions', 'ie 8'],
          cascade   : false
        },
        pixrem:{
          atrules: true,
          browsers: 'ie <= 8'
        }
      };

      require('./gulp/task/scss')(GULP, FOLDER, GLP, UTILS, gScss);





    /*----------------------------------
	  * TASK # SERVE
	  ----------------------------------*/
      gServer = {
        server:'./',
        startPath: '/build/',
        directory: true,
        reloadOnRestart: false,
        reloadDelay: 2000,
        port: 7000
      };
      require('./gulp/task/serve')(GULP, GLP, gServer);





    /*----------------------------------
	  * TASK # TEMPLATES
	  -----------------------------------*/
      gTemplates = {
        extension       : 'jade',
        nameCache       : 'tmpHTML',
        inheritanceOpt  : {
          basedir: 'src/templates/'
        },
        filterTemplates : 'views',
        templatesOpt    : {
          pretty: true
        }
      };
      require('./gulp/task/templates')(GULP, FOLDER, GLP, UTILS, gTemplates);









	  /*----------------------------------
	  * TASK # WATCH
	  ----------------------------------*/
      require('./gulp/task/watch')(GULP, GLP, FOLDER);




/*-------------------------------------------------
* TASK GROUP
*--------------------------------------------------*/

      /*-------------------------------------------------
      * TASK # DEFAULT
      *--------------------------------------------------*/
        GULP.task('default',['help']);




      /*-------------------------------------------------
      * TASK # DEVELOP
      *--------------------------------------------------*/
        GULP.task(
          'develop',
          '---' +
          '\n\t1.- Compila todo el desarrollo para que se genera por primera vez la carpeta de GULP.' +
          '\n\t2.- Levanta el entorno de desarrollo',
          develop
        );

        function develop(){
          GLP.runSequence(
            'clean:templates',
            'clean:scripts',
            'clean:css',
            'clean:fonts',
            'clean:assets',
            'copy:img',
            'copy:media',
            'copy:favicon',
            'copy:fonts',
            'copy:libsjs',
            'copy:libscss',
            'templates',
            'scss',
            'js',
            'cacheTemplates',
            'serve',
            'watch'
          );
        }



      /*-------------------------------------------------
      * TASK # DIST
      *--------------------------------------------------*/
        GULP.task(
          'dist',
          'Genera las versiones de distribución para entregar a cliente. Version Normal || Verion Minificada',
          dist
        );
        function dist(){
          GLP.runSequence(
            'clean:templates',
            'clean:scripts',
            'clean:css',
            'clean:fonts',
            'clean:assets',
            'copy:img',
            'copy:media',
            'copy:favicon',
            'copy:fonts',
            'templates',
            'scss',
            'js',
            'dist:normal',
            'js:min',
            'css:min',
            'dist:min'
          );
        }

      /*-------------------------------------------------
      * TASK # CLEAN:ALL
      *--------------------------------------------------*/
        GULP.task(
          'clean:all',
          'Ejecuta todas las tareas clean',
          cleanAll
        );

        function cleanAll(){
          GLP.runSequence(
            'clean:templates',
            'clean:scripts',
            'clean:css',
            'clean:fonts',
            'clean:assets'
          );
        }


      /*-------------------------------------------------
      * TASK # COPY:ALL
      *--------------------------------------------------*/
        GULP.task(
          'copy:all',
          'Ejecuta todas las tareas de copy',
          copyAll
        );

        function copyAll(){
          GLP.runSequence(
            'copy:img',
            'copy:media',
            'copy:favicon',
            'copy:fonts'
          );
        }
})();
