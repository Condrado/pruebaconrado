var dFrontJs = {};
var bp_x_small      = 	320;
var bp_small        = 	768;
var bp_medium       = 	992;
var bp_large        = 	1280;
var bp_min_large    = 	1400;
var bp_max_large    = 	3500;

var isDesktop = false;
var isTablet = false;

$(document).ready(function () {
  var widWin = window.innerWidth;
  tabs.init('[data-init="tabs"]', 1);
  cards.initRadio('[data-function="card-radio"]');
  collapse.init('[data-init="collapse"]', true, 0);


  if(widWin > bp_medium){
    if($('[data-init="cards"]').attr("class").indexOf("slick-initialized") > -1){
      $('[data-init="cards"]').slick("unslick");
    }
  } else {
    cards.init('[data-init="cards"]');
  }

});


$( window ).resize(function() {
  var widWin = window.innerWidth;

  if(widWin > bp_medium){
    if(!isDesktop && $('[data-init="cards"]').attr("class").indexOf("slick-initialized") > -1){
      isDesktop = true;
      isTablet = false;
      $('[data-init="cards"]').slick("unslick");
    }

  } else {
    if(!isTablet){
      isDesktop = false;
      isTablet = true;
      cards.init('[data-init="cards"]');
    }

  }

});
