/*
 * @description Funcion para inicializar el componente Tab de jQuery UI
 * @returns {Object} Devuelve la instancia del carrusel del Tab de jQuery UI.
 */

var tabs  = (function () {
  var init = function (element, tabInit) {
    var element = element ? element : 'false';
    if(element){

      var tabs = $(element).find(".wf-tabs__tab");
      var tabActive = $(element).find(".active");
      var idActive = tabActive.children().attr("data-tab");


      // alguna activa
      if(tabActive.length > 0) {
        activeContentTab(idActive)
      // en caso contrario activamso siempre la primera
      } else {
        $(tabs[tabInit]).addClass("active");
        idActive = $(tabs[tabInit]).children().attr("data-tab");
        activeContentTab(idActive)
      }

      clickTab(tabs);

    }
  };

  var clickTab = function(tabs) {
    $('[data-function="click-tab"]').click(function(e) {
      e.preventDefault();
      var idTab = $(this).attr("data-tab");
      activeContentTab(idTab);
      $(tabs).removeClass("active");
      $(this).parent().addClass("active");
  
      cards.init('[data-init="cards"]');
    })
  };

  var activeContentTab = function(idActive) {
    $('.wf-tabs__content').hide();
    $(idActive).show();
  }

  return {
    init: init
  };
})();

