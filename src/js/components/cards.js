/*
 * @description Funcion para inicializar el componente Tab de jQuery UI
 * @returns {Object} Devuelve la instancia del carrusel del Tab de jQuery UI.
 */

var cards  = (function () {
  var init = function (element) {
    var element = element ? element : 'false';
    if(element){
      var widWin = window.innerWidth;
      var bp_medium       = 	992;
  
      if ($('[data-init="cards"]').attr("class").indexOf("slick-initialized") > -1) {
        $('[data-init="cards"]').slick("unslick");
      }
      if(widWin < bp_medium) {
        $(element).slick({
          infinite: true,
          speed: 300,
          dots: true,
          arrows: false,
        });
      }
    }
  };

  var initRadio = function (element) {
    $(element).click(function () {
      $(this).find(".wf-cards__radio").prop("checked", true);
      $('.wf-cards__card').removeClass('active');
      $(this).addClass("active");
    })
  };

  return {
    init: init,
    initRadio: initRadio
  };
})();

