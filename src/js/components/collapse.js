/*
 * @description Funcion para inicializar el componente Tab de jQuery UI
 * @returns {Object} Devuelve la instancia del carrusel del Tab de jQuery UI.
 */

var collapse  = (function () {
  var init = function (element, isCollapseOthers, open) {
    var element = element ? element : 'false';
    if(element){
     
      var title = $(element).find("[data-function='click']");
      var titleOut = $(element).find("[data-function='clickOut']");
      
      // Open collapse
      if(typeof(open) != "undefined") {
        var collapses = $(element).find(".wf-collapse__collapse");
        if(collapses.length > 0) {
          $(collapses[open]).addClass("open");
        }
      }

      $(title).click(function () {
        var classActive = $(this).parent().attr("class");
        if(isCollapseOthers){
          $(".wf-collapse__collapse").removeClass("open");
          if(classActive.indexOf("open") > 0){
            $(this).parent().toggleClass( "open" );
          }
        }
        $(this).parent().toggleClass( "open" );
      });
  
      $(titleOut).click(function () {
        $(this).toggleClass( "open" );
        $(this).parent().next().toggleClass( "open" );
      })
    }
  };
  

  return {
    init: init
  };
})();

