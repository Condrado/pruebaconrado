
## INSTALL

1. Instalamos todas paquetes de NPM: 
	```
	$ npm install
	```
	> Creará una carpeta en la raiz llamada *node_modules* donde se guardarán todas las dependencias de desarrollo para el proyecto. 


2. Ejecutar entoerno desarrollo: 
	```
	$ gulp develop
	```
	> Para poder acceder al contenido ya compilado deberemos ejecutar esta tarea que lanza un navegador apuntando a la ruta http://localhost:7000/build/. Dentro de "build" tendremos el el desarrollo ya compilado. 

	
3. Estructura basica de carpeta

	```
	\assets
	\build
	\gulp
	\src
	.gitignore
	gulpfile.js
	package.json
	readme.md
	```
	
	> - assets: sera la carpeta qeu contenga todos nuestros recursos graficos, imagenes, videos, fuentes, etc
	> - buil: proyecto compilado
	> - gulp: ficheros de configuracion y tareas especificas de gulp
	> - src: carpeta de desarrollo
	> - .gitignore: fichero para  ignirar publicar contenidos del proyecto a GIT
	> - gulpfile.js: configuracion rincipal de gulp y definicion de tareas
	> - readme.md: fichero de informacion adicional para el proeyecto