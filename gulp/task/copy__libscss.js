/*---------------------------------------------
 * COPY:FAVICON CONFIGURATION TASK
 *----------------------------------------------*/

/*
 * Copy favicon.ico of assets and paste into folder build/
 */

(function(){
  'use strict';

  module.exports = function(GULP, GLP, FOLDER){
    GULP.task(
      'copy:libscss',
      'Copia las librerias javascript incluidas en src en build/css/libs',
      copyLibscss
    );

    function copyLibscss(){
      return GULP.src(GLP.path.join(FOLDER.src, FOLDER.scss, FOLDER.libs)+'**/*.*')
        .pipe(GULP.dest(GLP.path.join(FOLDER.build, FOLDER.css, FOLDER.libs)));
    }
  }
})();
