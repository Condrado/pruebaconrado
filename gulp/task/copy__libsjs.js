/*---------------------------------------------
 * COPY:FAVICON CONFIGURATION TASK
 *----------------------------------------------*/

/*
 * Copy favicon.ico of assets and paste into folder build/
 */

(function(){
  'use strict';

  module.exports = function(GULP, GLP, FOLDER){
    GULP.task(
      'copy:libsjs',
      'Copia las librerias javascript incluidas en src en build/js/libs',
      copyLibsjs
    );

    function copyLibsjs(){
      return GULP.src(GLP.path.join(FOLDER.src, FOLDER.js, FOLDER.libs)+'**/*.*')
        .pipe(GULP.dest(GLP.path.join(FOLDER.build, FOLDER.js, FOLDER.libs)));
    }
  }
})();
